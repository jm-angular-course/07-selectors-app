import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { combineLatest, Observable, of } from 'rxjs';
import { Country, SmallCountry } from '../interfaces/country.interface';

@Injectable({
  providedIn: 'root',
})
export class CountriesService {
  private BASE_URL: string = 'https://restcountries.eu/rest/v2';
  private _regions: string[] = [
    'Africa',
    'Americas',
    'Asia',
    'Europe',
    'Oceania',
  ];

  get regions(): string[] {
    return [...this._regions];
  }

  get params() {
    return new HttpParams().set('fields', 'name;alpha3Code');
  }

  constructor(private http: HttpClient) {}

  getCountriesByRegion(region: string): Observable<SmallCountry[]> {
    const url = `${this.BASE_URL}/region/${region}`;
    return this.http.get<SmallCountry[]>(url, { params: this.params });
  }

  getCountryByAlpha3Code(alpha3Code: string): Observable<Country | null> {
    if (!alpha3Code) return of(null);
    const url = `${this.BASE_URL}/alpha/${alpha3Code}`;
    return this.http.get<Country>(url);
  }

  getSmallCountryByAlpha3Code(alpha3Code: string): Observable<SmallCountry> {
    const url = `${this.BASE_URL}/alpha/${alpha3Code}`;
    return this.http.get<SmallCountry>(url, { params: this.params });
  }

  getCountriesByAlpha3Codes(alpha3Codes: string[]): Observable<SmallCountry[]> {
    if (!alpha3Codes || !alpha3Codes.length) return of([]);

    const requests: Observable<SmallCountry>[] = [];
    alpha3Codes.forEach((alpha3Code) => {
      const request = this.getSmallCountryByAlpha3Code(alpha3Code);
      requests.push(request);
    });

    return combineLatest(requests);
  }
}
