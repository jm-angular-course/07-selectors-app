import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { switchMap, tap } from 'rxjs/operators';
import { Country, SmallCountry } from '../../interfaces/country.interface';
import { CountriesService } from '../../services/countries.service';

@Component({
  selector: 'app-selector',
  templateUrl: './selector.component.html',
  styles: [],
})
export class SelectorComponent implements OnInit {
  myForm: FormGroup = this.fb.group({
    region: ['', Validators.required],
    country: ['', Validators.required],
    border: ['', Validators.required],
  });

  // llenar selectores
  regions: string[] = [];
  countries: SmallCountry[] = [];
  borders: SmallCountry[] = [];

  // UI
  loading: boolean = false;

  constructor(
    private fb: FormBuilder,
    private countriesService: CountriesService
  ) {}

  ngOnInit(): void {
    this.regions = this.countriesService.regions;

    // cuando cambie de región
    this.myForm
      .get('region')
      ?.valueChanges.pipe(
        tap((_) => {
          this.myForm.get('country')?.reset('');
          this.myForm.get('border')?.reset('');
          this.loading = true;
        }),
        switchMap((region: string) =>
          this.countriesService.getCountriesByRegion(region)
        )
      )
      .subscribe((countries) => {
        this.countries = countries;
        this.loading = false;
      });

    // cuando cambie de país
    this.myForm
      .get('country')
      ?.valueChanges.pipe(
        tap((_) => {
          this.borders = [];
          this.myForm.get('border')?.reset('');
          this.loading = true;
        }),
        switchMap((alpha3Code: string) =>
          this.countriesService.getCountryByAlpha3Code(alpha3Code)
        ),
        switchMap((country: Country | null) => {
          return this.countriesService.getCountriesByAlpha3Codes(
            country?.borders!
          );
        })
      )
      .subscribe((countries) => {
        this.borders = countries;
        this.loading = false;
      });
  }

  save() {
    console.log('save', this.myForm.value);
  }
}
